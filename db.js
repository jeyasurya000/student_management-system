var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'student_management'
});
connection.connect(function (err) {
    if (err) throw err;
    console.log('database is connected successfully !');
});
module.exports = connection;