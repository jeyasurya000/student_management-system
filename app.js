var express = require("express");
var bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const session = require('express-session');
// const busboy = require("then-busboy");
var cookieParser = require('cookie-parser');
var connection = require('./db.js');
const path = require('path');


var app = express();
var port = process.env.PORT || 3000;


app.use
    (session({
        name: 'session-id',
        secret: 'this is secret',
        resave: true,
        saveUninitialized: true,
        cookie: {
            maxAge: 900000,
            sameSite: true
        }

    }))



app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload());
app.use(cookieParser('abcdef-12345'))
app.set("view engine", "ejs");


app.engine('ejs', require('ejs').__express);
app.set('views', "./views")
app.get('/myProject', (req, res) => {
    res.render('index');
})




const validator = async(req, res, next) => {
    const schema = joi.object({
        user_name: joi.string().optional(),
        first_name: joi.string().optional(),
        last_name: joi.string().optional(),
        roll_number: joi.string().optional(),
        gender: joi.string().optional(),
        blood_group: joi.string().optional(),
        email_address: joi.string().email().required(),
        password: joi.string().min(6).required(),
        is_admin: joi.number().integer().required(),
        subject: joi.string().optional()
        

    })


    const value = await schema.validate(req.body);
    if (value.error) {
        res.json({
            success: 0,
            message: value.error.details[0].message
        })
    } else {
        next();
    }
}

/*********************************************TEACHER REGISTRATION*********************************************************************8 */


app.get('/teacher_signin', function (req, res) {
    res.render('teacher_signin');
});
app.post('/teacher_signin', validator, function (req, res, next) {
    

    connection.query('SELECT * FROM teacher WHERE email_address =?', [req.body.email_address], function (err, data, fields) {
        if (err) throw err
        if (data.length > 1) {
            var msg = req.body.email_address + "was already exist";
        } else {


            connection.query(`INSERT INTO teacher (user_name,gender,email_address,password,is_admin) VALUES ("${req.body.user_name}","${req.body.subject}","${req.body.gender}","${req.body.email_address}","${req.body.password}","${req.body.is_admin}")`, (err, output) => {
                if (err) throw err;
            });
            var msg = "Your are successfully registered";
        }
        res.render('teacher_signin',
            { alertMsg: msg 
    });
    })
    next;
});


/********************************************ADDING STUDENT *************************************************************************************** */

app.get('/teacher_login', (req, res) => {

    if (req.session.teacherId) {
        return res.redirect('/dashboard');
    }else{
    res.render('teacher_login');
    }

});
app.post('/teacher_login', function (req, res) {
    connection.query("call teacherlogin(?,?)" ,[req.body.email_address , req.body.password], (err, result, fields) => {

        if (err) throw err
        var results=result[0]
        if (results.length > 0) {
            req.cookies.teacherId = results[0].id;
            req.cookies.user = results[0];
            req.session.teacherId = results[0].id;
            req.session.user = results[0].user_name;
            //console.log(results[0].id);

            console.log(req.session);
            res.redirect("/dashboard");


        } else {
            res.render('teacher_login', { alertMsg: "Your Email Address or password is wrong" });

        }
    })
})

app.get('/logout', (req, res) => {
    req.session.destroy((err) => {
        //console.log("logout");
        res.redirect('/teacher_login');

    });

})


/*******************************************************student add************************************************************** */
app.get('/add_student', function (req, res) {

    connection.query("select cls,sec from class_section", function (err, result, fields) {
        
        if (err) throw err
    
        
        res.render('add_student', {
           dropdownVals:result

        })
    }) 
    
})


app.post('/add_student',function (req, res, next) {

    
    let uploadedFile = req.files.image;
    let image_name = uploadedFile.name;
    let fileExtension = uploadedFile.mimetype.split('/')[1];
    image_name = req.body.roll_number + '.' + fileExtension;

      
            // check the filetype before uploading it
            if (uploadedFile.mimetype === 'image/png' || uploadedFile.mimetype === 'image/jpeg' || uploadedFile.mimetype === 'image/gif') {
                // upload the file to the /public/assets/img directory
                uploadedFile.mv(`public/assets/img/${image_name}`, (err) => {
                    if (err) {
                        return res.status(500).send(err);
                    }
                
                    connection.query("CALL studentAdd(?,?,?,?,?,?,?,?,?,?,?)", [image_name,req.body.first_name, req.body.last_name, req.body.class, req.body.section, req.body.roll_number, req.body.gender, req.body.blood_group, req.body.email_address, req.body.password,  req.session.teacherId], (err, result) => {
                        if (err) {
                            

                            return res.status(500).send(err);
                            
                        }
                        
                        res.redirect('/dashboard');
                    });
                
                });
            
            }else {
                message = "Invalid File format. Only 'gif', 'jpeg' and 'png' images are allowed.";
                res.render('add_student.ejs', {
                    message,
                   
                });
            }
        

})
            

//****************************************DISPLAYS ALL DETAILS OF STUDENT IN DASHBOARD AFTER LOGIN***************************************************************8 */

app.get('/dashboard', function (req, res) {
    let getDetails = [];

    connection.query(`select is_admin from teacher where id="${req.session.teacherId}"` , function (error, data) {
        
        //console.log(results);
        if (data[0].is_admin == 0) {

            connection.query("call getdetail(?)",[req.session.teacherId], function (error, result) {
                if (error) throw error;
                let results = result[0];
                var i;
                for (i = 0; i < results.length; i++) {
                    let data = {
                        id: results[i].id,
                        image: results[i].image,
                        first_name: results[i].first_name,
                        last_name: results[i].last_name,
                        email_address: results[i].email_address,
                        password: results[i].password,
                        gender: results[i].gender,
                        blood_group: results[i].blood_group,
                        roll_number: results[i].roll_number,
                        section: results[i].section,
                        class: results[i].class,
                        teacher_id: results[i].user_name

                    }
                    getDetails.push(data)
                }
                //console.log("hellorrrrr");
                res.render('teacher profile', {
                    getDetails,
                    topicHead: 'users_details'
                });
            })
        } else {

            connection.query("call getting()", function (error, result) {
                if (error) throw error;
               var results=result[0];
                var i;
                for (i = 0; i < results.length; i++) {
                    let data = {
                        id: results[i].id,
                        image: results[i].image,
                        first_name: results[i].first_name,
                        last_name: results[i].last_name,
                        email_address: results[i].email_address,
                        password: results[i].password,
                        gender: results[i].gender,
                        blood_group: results[i].blood_group,
                        roll_number: results[i].roll_number,
                        section: results[i].section,
                        class: results[i].class,
                        teacher_id: results[i].teacher_id
                    }
                    getDetails.push(data)
                }

               
                res.render('teacher profile', {
                    getDetails,
                    topicHead: 'users_details'
                });
            })
        }
        //console.log(getDetails);
        
    })

});
app.post('/select', function (req, res) {
var test =[]
    connection.query(`select user_name from teacher where id= "${req.session.teacherId}`, function (err, result, fields) {
        
        if (err) throw err
        var i;
        for (i = 0; i < result.length; i++) {
            let data = {
                id: result[i].user_name
            }
            test.push(data)
        }
        
        res.render('teacher profile', {
            test
          

        })
    }) 
    
})
app.post('/dashboards', (req, res) => {
    var getDetails = [];
    
        connection.query(`select * from student where id="${req.body.search}" or first_name="${req.body.search}" or last_name="${req.body.search}"or class="${req.body.search}" or section="${req.body.search}" or roll_number="${req.body.search}" or gender="${req.body.search}" or  gender="${req.body.blood_group}" or email_address="${req.body.search}"  `, function (error, results) {

            if (error) throw error;
            var i;
            for (i = 0; i < results.length; i++) {
                let data = {
                    id: results[i].id,
                    image: results[i].image,
                    first_name: results[i].first_name,
                    last_name: results[i].last_name,
                    email_address: results[i].email_address,
                    password: results[i].password,
                    gender: results[i].gender,
                    blood_group: results[i].blood_group,
                    roll_number: results[i].roll_number,
                    section: results[i].section,
                    class: results[i].class

                }
                getDetails.push(data)
            }
            //console.log("hellorrrrr");
            res.render('teacher profile', {
                getDetails,
                topicHead: 'users_details'
            });
        })
    
})

/**************************************************UPDATE STUDENTS DETAILS************************************************************************ */

app.get('/edit/:id', function (req, res, next) {

    var sql = `SELECT * FROM student WHERE id=${req.params.id}`;
    connection.query(sql, function (err, data) {
        if (err) throw err;

        res.render('update_box', { editData: data[0] });
    });
});

app.post('/edits/:id', function (req, res, next) {
    var id = req.params.id;
    var updateData = req.body;
    var sql = `UPDATE student SET ? WHERE id= ?`;
    connection.query(sql, [updateData, id], function (err, data) {
        if (err) throw err;
        //console.log(data.affectedRows + " record(s) updated");
    });
    res.redirect('/dashboard');
});


/*************************************TO DELETE THE STUDENT DETAILS FROM STUDENT TABLE*********************************************************************************** */
app.get('/delete/:id', function (req, res, next) {
    var id = req.params.id;
    
    var sql = "CALL deletedata(?)";
    connection.query(sql, [id], function (err, data) {
        if (err) throw err;
        //console.log(data.affectedRows + " record(s) updated");
    });
    res.redirect('/dashboard');

});
/*********************************************DISPLAY ALL DETAILS OF STUDENT WITH THEIR MARKS********************************************************************* */



app.get('/marks&details/:id', function (req, res) {
    var getDetails = [];
    
   
    connection.query("CALL getAll(?)", [req.params.id], (error, result)=> {
        if (error) throw error;
     let results = result[0];
        var i;
       // //console.log("length",results.length);
        for (i =0 ; i < results.length; i++) {
            let data = {
                id: results[i].id,
                image: results[i].image,
                first_name: results[i].first_name,
                last_name: results[i].last_name,
                section: results[i].section,
                class: results[i].class,
                email_address: results[i].email_address,
                password: results[i].password,
                gender: results[i].gender,
                standard: results[i].standard,
                blood_group: results[i].blood_group,
                roll_number: results[i].roll_number,
                Exam: results[i].Exam,
                English: results[i].English,
                Tamil: results[i].Tamil,
                Maths: results[i].Maths,
                Science: results[i].Science,
                Social_Science: results[i].Social_Science

            }
           
            getDetails.push(data)
console.log(results[i]);
            //console.log(data)
        }
        //console.log(getDetails)
       
        res.render('box', {
            getDetails

        });

 
        // //console.log(getDetails)
        // //console.log(results[0]);
        // //console.log("test");
    });
});

/********************************************ADD MARKS TO MARK TABLE******************************************************************** */

app.get('/addMarks', function (req, res) {
   
         res.render('addmarks');
    
});
   
app.post('/mark_table', function (req, res, next) {

    connection.query("call markTable(?,?,?,?,?,?,?)" ,[req.body.student_id,req.body.Exam,req.body.English,req.body.Tamil,req.body.Maths,req.body.Science,req.body.Social_Science], (err, output) => {
        if (err) throw err;
    });
    //console.log(req.session.teacherId);

    var msg = "Your are successfully registered";

    res.redirect('/dashboard');

    next;
});


/******************************************************************************************************************** */




app.get('/editMark/:id', function (req, res, next) {

    var sql = `SELECT * FROM marks WHERE student_id=${req.params.id }`;
    connection.query(sql, function (err, data) {
        if (err) throw err;

        res.render('update mark', { editData: data[0] });
    });
});

app.post('/editMark/:id', function (req, res, next) {
    var id = req.params.id
    var updateData = req.body;
    var sql = `UPDATE marks  SET ? WHERE student_id= ?`;
    connection.query(sql, [updateData, id], function (err, data) {
        if (err) throw err;
        console.log(data.affectedRows + " record(s) updated");
    });
    res.redirect('/dashboard');
});





/*********************************************************************************************************************8 */
app.get('/student_login', function (req, res) {

    res.render('student_login');

});
app.post('/student_login', function (req, res) {
    connection.query("call studentlogin(?,?)",[req.body.email_address,req.body.password] ,(err, result, fields) => {

        if (err) throw err
        var results=result[0];
        if (results.length > 0) {
            req.session.studentId = results[0].id;
            
                
                //console.log(results[0].id);
    
            //console.log(results[0].student_id);


            res.redirect("/marks");


        } else {
            res.render('student_login', { alertMsg: "Your Email Address or password is wrong" });

        }
    })
})

app.get('/stdlogout', (req, res) => {
    req.session.destroy((err) => {
        //console.log("logout");
        res.redirect('/student_login');

    });

})
 
/*******************************************************************************************************************************************8 */
app.get('/marks', function (req, res) {

    var getDetails = [];
    connection.query(`select * from marks where student_id="${req.session.studentId}"`, function (error, results, fields) {
        if (error) throw error;
        var i;
        for (i = 0; i < results.length; i++) {

            let data = {
                student_id: results[i].student_id,
                English: results[i].English,
                Tamil: results[i].Tamil,
                Maths: results[i].Maths,
                Science: results[i].Science,
                Social_Science: results[i].Social_Science

            }
            getDetails.push(data)

        }
        //console.log("test");
        res.render('mark_page', {
            getDetails


        });


    });
});

app.listen(port, () => {
    //console.log(`Server is listening on port ${port}`);
});


